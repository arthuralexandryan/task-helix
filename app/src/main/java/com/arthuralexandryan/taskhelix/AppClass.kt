package com.arthuralexandryan.taskhelix

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AppClass: Application() {
}