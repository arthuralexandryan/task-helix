package com.arthuralexandryan.taskhelix.api.model

data class NewsResponse(
    val category: String,
    val title: String,
    val body: String,
    val shareUrl: String,
    val coverPhotoUrl: String,
    val date: Long,
    val gallery: List<GalleryResponse>
)