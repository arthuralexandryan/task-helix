package com.arthuralexandryan.taskhelix.api.mapper

import com.arthuralexandryan.taskhelix.api.dto.NewsDto
import com.arthuralexandryan.taskhelix.api.model.ModelResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import retrofit2.Response

object ApiMapper {
    fun mapToListNewsDto(json: Response<ModelResponse>): List<NewsDto>? =
            json.body()?.metadata?.map {
                NewsDto(
                    category = it.category,
                    title = it.title,
                    coverPhotoUrl = it.coverPhotoUrl,
                    date = it.date
                )
            }
}