package com.arthuralexandryan.taskhelix.api.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsDetailDto(
    val category: String?,
    val title: String?,
    val coverPhotoUrl: String?,
    val date: Long
): Parcelable
