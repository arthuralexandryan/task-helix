package com.arthuralexandryan.taskhelix.api.dto

data class NewsDto(
    val category: String?,
    val title: String?,
    val coverPhotoUrl: String?,
    val date: Long
)
