package com.arthuralexandryan.taskhelix.api.model

data class GalleryResponse(
    val title: String,
    val thumbnailUrl: String,
    val contentUrl: String
)
