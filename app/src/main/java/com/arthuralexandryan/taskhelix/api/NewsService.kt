package com.arthuralexandryan.taskhelix.api

import com.arthuralexandryan.taskhelix.api.model.ModelResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {

    @GET("temp/json.php")
    suspend fun getJson(): Response<ModelResponse>
}