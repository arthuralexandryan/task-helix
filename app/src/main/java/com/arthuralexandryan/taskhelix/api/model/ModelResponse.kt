package com.arthuralexandryan.taskhelix.api.model

import java.lang.reflect.Array

data class ModelResponse(
    val success: Boolean,
    val errors: List<Any>,
    val internal_errors: List<Any>,
    val metadata: List<NewsResponse>
)
