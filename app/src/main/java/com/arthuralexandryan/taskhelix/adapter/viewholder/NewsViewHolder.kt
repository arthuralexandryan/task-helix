package com.arthuralexandryan.taskhelix.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.arthuralexandryan.taskhelix.api.dto.NewsDto
import com.arthuralexandryan.taskhelix.databinding.ItemNewsBinding
import com.arthuralexandryan.taskhelix.di.GlideApp
import com.bumptech.glide.Glide
import java.util.*
import javax.inject.Inject

class NewsViewHolder @Inject constructor(private val binding: ItemNewsBinding) :
    RecyclerView.ViewHolder(binding.root) {

    lateinit var itemClick: () -> Unit

    fun onBind(newsDto: NewsDto) {
        initItemClick()
        binding.title.text = newsDto.title
        binding.category.text = newsDto.category
        binding.date.text = Date(newsDto.date).toString()
        GlideApp.with(binding.coverPhoto.context)
            .load(newsDto.coverPhotoUrl)
            .into(binding.coverPhoto)
    }

    private fun initItemClick() {
        binding.itemLayout.setOnClickListener {
            itemClick.invoke()
        }
    }
}