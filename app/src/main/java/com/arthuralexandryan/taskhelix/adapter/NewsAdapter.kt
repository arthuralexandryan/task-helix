package com.arthuralexandryan.taskhelix.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arthuralexandryan.taskhelix.adapter.viewholder.NewsViewHolder
import com.arthuralexandryan.taskhelix.api.dto.NewsDto
import com.arthuralexandryan.taskhelix.databinding.ItemNewsBinding
import javax.inject.Inject

class NewsAdapter @Inject constructor(): RecyclerView.Adapter<NewsViewHolder>() {
    var listNews: List<NewsDto> = emptyList()
    lateinit var itemClick: (Int) -> Unit
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder =
        NewsViewHolder(ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.onBind(listNews[position])
        holder.itemClick = {
            itemClick.invoke(position)
        }
    }

    override fun getItemCount(): Int = listNews.size
}