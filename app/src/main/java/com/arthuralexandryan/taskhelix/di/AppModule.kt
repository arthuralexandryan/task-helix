package com.arthuralexandryan.taskhelix.di

import com.arthuralexandryan.taskhelix.api.NewsService
import com.arthuralexandryan.taskhelix.repository.NewsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
object AppModule {

    @Provides
    fun provideAudioRepositoryImpl(
        service: NewsService
    ): NewsRepositoryImpl = NewsRepositoryImpl(service)
}