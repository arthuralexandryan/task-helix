package com.arthuralexandryan.taskhelix.ui

import androidx.fragment.app.viewModels
import com.arthuralexandryan.taskhelix.adapter.NewsAdapter
import com.arthuralexandryan.taskhelix.api.dto.NewsDetailDto
import com.arthuralexandryan.taskhelix.databinding.FragmentNewsDetailsBinding
import com.arthuralexandryan.taskhelix.di.GlideApp
import com.arthuralexandryan.taskhelix.ui.base.BaseFragment
import com.arthuralexandryan.taskhelix.ui.viewmodel.NewsViewModel
import com.arthuralexandryan.taskhelix.utils.AppConstants.KEY_NEWS_DETAILS
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class NewsDetailsFragment : BaseFragment<FragmentNewsDetailsBinding>() {

    private val viewModel: NewsViewModel by viewModels()

    @Inject
    lateinit var newsAdapter: NewsAdapter


    override fun getViewBinding(): FragmentNewsDetailsBinding = FragmentNewsDetailsBinding.inflate(layoutInflater)

    override fun onBindView(binding: FragmentNewsDetailsBinding?) {
        val newsDetails = getNewsDetails()
        binding?.title?.text =  newsDetails?.title
        binding?.category?.text =  newsDetails?.category
        binding?.date?.text = newsDetails?.date?.let { Date(it).toString() }
        binding?.coverPhoto?.let {
            GlideApp.with(this)
                .load(newsDetails?.coverPhotoUrl)
                .into(it)
        }
    }

    private fun getNewsDetails() = arguments?.getParcelable<NewsDetailDto>(KEY_NEWS_DETAILS)
}