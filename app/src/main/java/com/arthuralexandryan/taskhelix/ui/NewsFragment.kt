package com.arthuralexandryan.taskhelix.ui

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.arthuralexandryan.taskhelix.R
import com.arthuralexandryan.taskhelix.adapter.NewsAdapter
import com.arthuralexandryan.taskhelix.api.dto.NewsDetailDto
import com.arthuralexandryan.taskhelix.api.mapper.ApiMapper
import com.arthuralexandryan.taskhelix.databinding.FragmentNewsBinding
import com.arthuralexandryan.taskhelix.ui.base.BaseFragment
import com.arthuralexandryan.taskhelix.ui.viewmodel.NewsViewModel
import com.arthuralexandryan.taskhelix.utils.AppConstants.KEY_NEWS_DETAILS
import com.arthuralexandryan.taskhelix.utils.hide
import com.arthuralexandryan.taskhelix.utils.initRecycleView
import com.arthuralexandryan.taskhelix.utils.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@AndroidEntryPoint
class NewsFragment : BaseFragment<FragmentNewsBinding>() {

    private val viewModel: NewsViewModel by viewModels()

    @Inject
    lateinit var newsAdapter: NewsAdapter

    override fun getViewBinding(): FragmentNewsBinding = FragmentNewsBinding.inflate(layoutInflater)

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindView(binding: FragmentNewsBinding?) {
        initRecyclerViewNews(binding)
        initAdapterItemClick()
        binding?.loadingProcess?.show()
        lifecycleScope.launchWhenResumed {
            viewModel.getNews().collectLatest { response ->
                response?.run {
                    newsAdapter.listNews = ApiMapper.mapToListNewsDto(response) ?: emptyList()
                    newsAdapter.notifyDataSetChanged()
                    binding?.loadingProcess?.hide()
                }
            }
        }
    }

    private fun initAdapterItemClick() {
        newsAdapter.itemClick = {
            handleItemClick(it)
        }
    }

    private fun handleItemClick(position: Int) {
        val bundle = Bundle()
        newsAdapter.listNews[position].run {
            bundle.putParcelable(
                KEY_NEWS_DETAILS, NewsDetailDto(
                    category = category,
                    title = title,
                    coverPhotoUrl = coverPhotoUrl,
                    date = date
                )
            )
        }
        findNavController().navigate(R.id.nav_newsDetailsFragment, bundle)
    }

    private fun initRecyclerViewNews(binding: FragmentNewsBinding?) {
        binding?.userList?.initRecycleView(
            appAdapter = newsAdapter,
        )
    }


}