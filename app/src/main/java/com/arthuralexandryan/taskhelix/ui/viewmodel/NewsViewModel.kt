package com.arthuralexandryan.taskhelix.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.arthuralexandryan.taskhelix.api.dto.NewsDto
import com.arthuralexandryan.taskhelix.api.model.ModelResponse
import com.arthuralexandryan.taskhelix.repository.NewsRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class NewsViewModel @Inject constructor(
    private val repositoryImpl: NewsRepositoryImpl
): ViewModel() {
    fun getNews(): StateFlow<Response<ModelResponse>?> {
        val data = MutableStateFlow<Response<ModelResponse>?>(null)
        viewModelScope.launch {
            repositoryImpl.getNews().collect { response ->
                data.value = response
            }
        }
        return data
    }
}