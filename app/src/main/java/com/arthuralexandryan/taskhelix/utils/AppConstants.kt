package com.arthuralexandryan.taskhelix.utils

object AppConstants {
    const val BASE_URL: String = "https://www.helix.am/"

    const val KEY_NEWS_DETAILS = "news_details"
}