package com.arthuralexandryan.taskhelix.utils

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


fun <Adapter> RecyclerView.initRecycleView(
    appAdapter: Adapter,
    isVertical: Boolean = true,
    isSizeFixed: Boolean = true
) {
    layoutManager = LinearLayoutManager(
        context,
        if (isVertical) LinearLayoutManager.VERTICAL else LinearLayoutManager.HORIZONTAL,
        false
    )
    adapter = appAdapter as RecyclerView.Adapter<*>
    isNestedScrollingEnabled = false
    setHasFixedSize(isSizeFixed)
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}