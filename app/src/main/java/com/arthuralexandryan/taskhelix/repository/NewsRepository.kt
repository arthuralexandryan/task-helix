package com.arthuralexandryan.taskhelix.repository

import com.arthuralexandryan.taskhelix.api.dto.NewsDto
import com.arthuralexandryan.taskhelix.api.model.ModelResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface NewsRepository {
    suspend fun getNews(): Flow<Response<ModelResponse>>
}