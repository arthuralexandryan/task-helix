package com.arthuralexandryan.taskhelix.repository

import com.arthuralexandryan.taskhelix.api.NewsService
import com.arthuralexandryan.taskhelix.api.dto.NewsDto
import com.arthuralexandryan.taskhelix.api.mapper.ApiMapper
import com.arthuralexandryan.taskhelix.api.model.ModelResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import retrofit2.Response
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val service: NewsService
): NewsRepository{
    override suspend fun getNews(): Flow<Response<ModelResponse>> = flowOf(service.getJson())
}